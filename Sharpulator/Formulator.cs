﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Sharpulator
{
    public partial class SharpulatorForm : Form
    {
        public SharpulatorForm()
        {
            InitializeComponent();
        }

        #region  сбросокнопки
        private void btCE_Click(object sender, EventArgs e)  //OK
        {
            //сброс последнего операнда
            if (PointThere) PointThere = false;
            if (DisplaySign) DisplaySign = false;
            OperandB = 0.0;
            this.PCdisplay.Text = "0,";            
        }

        private void btC_Click(object sender, EventArgs e)      //OK
        {
            //полный сброс
            if (PointThere) PointThere = false;
            if (DisplaySign) DisplaySign = false;
            OperandA = 0.0;
            OperandB = 0.0;
            this.PCdisplay.Text = "0,";    
        }

        private void btBackspace_Click(object sender, EventArgs e)      //OK
        {
            //стереть последнюю цифру с экрана (или обнулить, если она одна)                    
            string CurrentCifers = PCdisplay.Text.ToString();

            //отслеживание запятой в записи
            
            if (CurrentCifers.EndsWith(",") && PointThere)
            {//последний символ - десятичная запятая и введённое число дробное

                PointThere = false;
                return;
            }

            if (CurrentCifers.Length == 2) 
            {
                if (CurrentCifers == "0,")
                    return;
                else
                {
                    this.PCdisplay.Text = "0,";
                    return;
                }
            }

            if (!PointThere)
            {
                string NewText = CurrentCifers.Remove((CurrentCifers.Length - 2), 1);        
                this.PCdisplay.Text = NewText;
            }
            else
            {
                string NewText = CurrentCifers.Remove((CurrentCifers.Length - 1), 1);           
                this.PCdisplay.Text = NewText;
            }



        }

        #endregion

        #region математические функции


        private void btSqrt_Click(object sender, EventArgs e)
        {
            //корень квадратный
            string Display = PCdisplay.Text.ToString();
            try
            {
                OperandA = Convert.ToDouble(Display);
                double result = Math.Sqrt(OperandA);
                OperandA = result;
                //нужно отслеживать точку!!!!!

                //===== при дохера жмяков пропадает запятон в конце!

                double checkPoint = result % 1;
                if (checkPoint != 0)
                    PointThere = true;

                if (!PointThere)
                    PCdisplay.Text = OperandA.ToString() + ",";
                else
                    PCdisplay.Text = OperandA.ToString();
            }
            catch (FormatException)
            {
                System.Windows.Forms.MessageBox.Show("Бида-00! в диспелее абы што, и калькулшлятор подавилсо!");
            }
            catch (OverflowException)
            {
                System.Windows.Forms.MessageBox.Show("Бида-а-а! в диспелее абы што, и калькулшлятор подавилсо!");
            }
        }

        private void btPrCnt_Click(object sender, EventArgs e)
        {
            //процент
        }

        private void btReverse_Click(object sender, EventArgs e)
        {
            //число, обратное данному
        }

        private void btEqual_Click(object sender, EventArgs e)
        {
            //РАВНО: посчитать если 1 опернад, если нет операндов, если 2 операнда
        }

        private void btDivide_Click(object sender, EventArgs e)
        {
            //делить
        }

        private void btMultip_Click(object sender, EventArgs e)
        {
            //размножать
        }

        private void btMinus_Click(object sender, EventArgs e)
        {
            //отнимать
        }

        private void btPlus_Click(object sender, EventArgs e)
        {
            //добавлять
        }

        #endregion

        #region цифрокнопки

        private void btPoint_Click(object sender, EventArgs e)  //OK
        {
            //в деся-дроби, добавить в строку                   

            if (!PointThere) PointThere = true;
            else return;
        }

        private void btSign_Click(object sender, EventArgs e)   //OK
        {                                                    //сменить знак числа
            if (DisplaySign) DisplaySign = false;
            else DisplaySign = true;

            //контроль длины поля                                           ========  тут это не нужно!!!!!!!!!!
            /*      if (this.PCdisplay.Text.Length == DISPLAY_DIGIT) return;                      */

            //сменить знак числа в дисплее

            string CurrentCifers = this.PCdisplay.Text.ToString();

            if (CurrentCifers == "0,") return;
            else
            {
                if (DisplaySign)
                {
                    this.PCdisplay.Text = "-";
                    this.PCdisplay.Text += CurrentCifers;
                }
                else
                {
                    if (CurrentCifers.Contains("-"))
                    {
                        string NewText = CurrentCifers.Remove(0, 1);
                        //System.Windows.Forms.MessageBox.Show(NewText);        //debug mbox
                        this.PCdisplay.Text = NewText;
                    }

                }
            }
        }

        private void btZero_Click(object sender, EventArgs e)       //ok
        {
            //контроль длины поля
            if (!DisplaySign && this.PCdisplay.Text.Length == DISPLAY_DIGIT) return;
            if (DisplaySign && this.PCdisplay.Text.Length == (DISPLAY_DIGIT + 1)) return;

            //написать нуль, если...

            if (PointThere) { this.PCdisplay.Text += "0"; return; }

            string CurrentCifers = this.PCdisplay.Text.ToString();
            if (CurrentCifers == "0,") return;
            else this.PCdisplay.Text += "0,";
        }

        private void btOne_Click(object sender, EventArgs e)    //ok
        {
            //контроль длины поля
            if (!DisplaySign && this.PCdisplay.Text.Length == DISPLAY_DIGIT) return;
            if (DisplaySign && this.PCdisplay.Text.Length == (DISPLAY_DIGIT + 1)) return;

            //адын
            if (PointThere) this.PCdisplay.Text += "1";
            else
            {
                if (this.PCdisplay.Text.ToString() == "0,")
                {
                    PCdisplay.Text = "1,";
                    return;
                }
                else
                {           //OK
                    string CurrentCifers = PCdisplay.Text.ToString();
                    string NewText = CurrentCifers.Remove((CurrentCifers.Length - 1), 1);
                    this.PCdisplay.Text = NewText;
                    this.PCdisplay.Text += "1,";
                }
            }

        }

        private void btTwo_Click(object sender, EventArgs e)    //ok
        {
            //контроль длины поля
            if (!DisplaySign && this.PCdisplay.Text.Length == DISPLAY_DIGIT) return;
            if (DisplaySign && this.PCdisplay.Text.Length == (DISPLAY_DIGIT + 1)) return;

            //два
            if (PointThere) this.PCdisplay.Text += "2";
            else
            {
                if (this.PCdisplay.Text.ToString() == "0,")
                {
                    PCdisplay.Text = "2,";
                    return;
                }
                else
                {           //OK
                    string CurrentCifers = PCdisplay.Text.ToString();
                    string NewText = CurrentCifers.Remove((CurrentCifers.Length - 1), 1);
                    this.PCdisplay.Text = NewText;
                    this.PCdisplay.Text += "2,";
                }
            }
        }

        private void btThree_Click(object sender, EventArgs e)  //ok
        {
            //контроль длины поля
            if (!DisplaySign && this.PCdisplay.Text.Length == DISPLAY_DIGIT) return;
            if (DisplaySign && this.PCdisplay.Text.Length == (DISPLAY_DIGIT + 1)) return;

            //тры
            if (PointThere) this.PCdisplay.Text += "3";
            else
            {
                if (this.PCdisplay.Text.ToString() == "0,")
                {
                    PCdisplay.Text = "3,";
                    return;
                }
                else
                {           //OK
                    string CurrentCifers = PCdisplay.Text.ToString();
                    string NewText = CurrentCifers.Remove((CurrentCifers.Length - 1), 1);
                    this.PCdisplay.Text = NewText;
                    this.PCdisplay.Text += "3,";
                }
            }
        }

        private void btFour_Click(object sender, EventArgs e)   //ok
        {
            //контроль длины поля
            if (!DisplaySign && this.PCdisplay.Text.Length == DISPLAY_DIGIT) return;
            if (DisplaySign && this.PCdisplay.Text.Length == (DISPLAY_DIGIT + 1)) return;

            //чешытыре
            if (PointThere) this.PCdisplay.Text += "4";
            else
            {
                if (this.PCdisplay.Text.ToString() == "0,")
                {
                    PCdisplay.Text = "4,";
                    return;
                }
                else
                {           //OK
                    string CurrentCifers = PCdisplay.Text.ToString();
                    string NewText = CurrentCifers.Remove((CurrentCifers.Length - 1), 1);
                    this.PCdisplay.Text = NewText;
                    this.PCdisplay.Text += "4,";
                }
            }
        }

        private void btFive_Click(object sender, EventArgs e)   //ok
        {
            //контроль длины поля
            if (!DisplaySign && this.PCdisplay.Text.Length == DISPLAY_DIGIT) return;
            if (DisplaySign && this.PCdisplay.Text.Length == (DISPLAY_DIGIT + 1)) return;

            //фити-пять
            if (PointThere) this.PCdisplay.Text += "5";
            else
            {
                if (this.PCdisplay.Text.ToString() == "0,")
                {
                    PCdisplay.Text = "5,";
                    return;
                }
                else
                {           //OK
                    string CurrentCifers = PCdisplay.Text.ToString();
                    string NewText = CurrentCifers.Remove((CurrentCifers.Length - 1), 1);
                    this.PCdisplay.Text = NewText;
                    this.PCdisplay.Text += "5,";
                }
            }
        }

        private void btSix_Click(object sender, EventArgs e)    //ok
        {
            //контроль длины поля
            if (!DisplaySign && this.PCdisplay.Text.Length == DISPLAY_DIGIT) return;
            if (DisplaySign && this.PCdisplay.Text.Length == (DISPLAY_DIGIT + 1)) return;

            //жесте-шесть
            if (PointThere) this.PCdisplay.Text += "6";
            else
            {
                if (this.PCdisplay.Text.ToString() == "0,")
                {
                    PCdisplay.Text = "6,";
                    return;
                }
                else
                {           //OK
                    string CurrentCifers = PCdisplay.Text.ToString();
                    string NewText = CurrentCifers.Remove((CurrentCifers.Length - 1), 1);
                    this.PCdisplay.Text = NewText;
                    this.PCdisplay.Text += "6,";
                }
            }
        }

        private void btSeven_Click(object sender, EventArgs e)  //ok
        {
            //контроль длины поля
            if (!DisplaySign && this.PCdisplay.Text.Length == DISPLAY_DIGIT) return;
            if (DisplaySign && this.PCdisplay.Text.Length == (DISPLAY_DIGIT + 1)) return;

            //семя-семь
            if (PointThere) this.PCdisplay.Text += "7";
            else
            {
                if (this.PCdisplay.Text.ToString() == "0,")
                {
                    PCdisplay.Text = "7,";
                    return;
                }
                else
                {           //OK
                    string CurrentCifers = PCdisplay.Text.ToString();
                    string NewText = CurrentCifers.Remove((CurrentCifers.Length - 1), 1);
                    this.PCdisplay.Text = NewText;
                    this.PCdisplay.Text += "7,";
                }
            }
        }

        private void btEight_Click(object sender, EventArgs e)  //ok
        {
            //контроль длины поля
            if (!DisplaySign && this.PCdisplay.Text.Length == DISPLAY_DIGIT) return;
            if (DisplaySign && this.PCdisplay.Text.Length == (DISPLAY_DIGIT + 1)) return;

            //восемь-просим
            if (PointThere) this.PCdisplay.Text += "8";
            else
            {
                if (this.PCdisplay.Text.ToString() == "0,")
                {
                    PCdisplay.Text = "8,";
                    return;
                }
                else
                {           //OK
                    string CurrentCifers = PCdisplay.Text.ToString();
                    string NewText = CurrentCifers.Remove((CurrentCifers.Length - 1), 1);
                    this.PCdisplay.Text = NewText;
                    this.PCdisplay.Text += "8,";
                }
            }
        }

        private void btNine_Click(object sender, EventArgs e)   //ok
        {
            //контроль длины поля
            if (!DisplaySign && this.PCdisplay.Text.Length == DISPLAY_DIGIT) return;
            if (DisplaySign && this.PCdisplay.Text.Length == (DISPLAY_DIGIT + 1)) return;

            //девьять
            if (PointThere) this.PCdisplay.Text += "9";
            else
            {
                if (this.PCdisplay.Text.ToString() == "0,")
                {
                    PCdisplay.Text = "9,";
                    return;
                }
                else
                {           //OK
                    string CurrentCifers = PCdisplay.Text.ToString();
                    string NewText = CurrentCifers.Remove((CurrentCifers.Length - 1), 1);
                    this.PCdisplay.Text = NewText;
                    this.PCdisplay.Text += "9,";
                }
            }
        }

        #endregion

        #region регистрокнопки

        private void btMC_Click(object sender, EventArgs e)     //OK
        {       //"очистить память"
            //стереть регистр 
            RegisterValue = false;
            MemRegister = 0.0;
            this.richTextBox_MemStat.Text = "";
        }

        private void btMR_Click(object sender, EventArgs e)     //OK
        {       //"вызвать число из памяти"
            //вызвать из регистра на дисплей
            if (RegisterValue)
            {
                string fromMem = MemRegister.ToString();
                if (!PointThere)
                    PCdisplay.Text = fromMem + ",";
                else
                    PCdisplay.Text = fromMem;
            }
            else return;
        }

        private void btMS_Click(object sender, EventArgs e)     //OK
        {       //"занести отображаемое число в память"
            //сохранить в регистре поверх с экрана

            string Display = PCdisplay.Text.ToString();

            try
            {
                MemRegister = Convert.ToDouble(Display);
                RegisterValue = true;
                this.richTextBox_MemStat.Text = "M";

            }
            catch (FormatException)
            {
                System.Windows.Forms.MessageBox.Show("Бида-а-а! в диспелее абы што, и в память оно не влезло!");
                RegisterValue = false;
                this.richTextBox_MemStat.Text = "";
                return;
            }
            catch (OverflowException)
            {
                System.Windows.Forms.MessageBox.Show("Бида-а-а! в диспелее абы што, и в память оно не влезло!");
                RegisterValue = false;
                this.richTextBox_MemStat.Text = "";
                return;
            }

        }

        private void btMplus_Click(object sender, EventArgs e)      //OK
        {       //"сложить отображаемое число с числом, хранящимся в памяти"
            //добавить к результату в регистре с экрана

            string Display = PCdisplay.Text.ToString();

            try
            {

                if (RegisterValue)
                {
                    MemRegister += Convert.ToDouble(Display);
                }
                else
                {
                    MemRegister = Convert.ToDouble(Display);
                }

                RegisterValue = true;
                this.richTextBox_MemStat.Text = "M";
            }
            catch (FormatException)
            {
                System.Windows.Forms.MessageBox.Show("Бида-а-а! в диспелее абы што, и в память оно не влезло!");
                RegisterValue = false;
                this.richTextBox_MemStat.Text = "";
                return;
            }
            catch (OverflowException)
            {
                System.Windows.Forms.MessageBox.Show("Бида-а-а! в диспелее абы што, и в память оно не влезло!");
                RegisterValue = false;
                this.richTextBox_MemStat.Text = "";
                return;
            }


        }
        
        #endregion

        #region действия меню

        private void ToolStripMenuItem_HelpSOS_Click(object sender, EventArgs e)
        {
            MessageBox.Show("ВИРУС! Вирус! Получите!");
        }

        private void ToolStripMenuItem_HelpAbout_Click(object sender, EventArgs e)
        {
            MessageBox.Show("ЭТА ПРОГРАММА пересохраняется всеми безавторскими кривами и обязанностями! КЫШ!");
        }

        private void ToolStripMenuItem_ViewUsual_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Вы выбрали режим калькулятора для БЛОНДИНОК!");
        }

        private void ToolStripMenuItem_ViewEngineer_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Вы выбрали режим калькулятора для ПРОШАРЕННЫХ пацанов!");
        }

        private void ToolStripMenuItem_ViewDigGroups_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Вы выбрали режим ГРУППИРОВАНИЯ цифирок!");
        }

        private void ToolStripMenuItem_EditCopy_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Вы хотите нарушить авторское право КОМПИРОВАНИЕМ!");
        }

        private void ToolStripMenuItem_EditPaste_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Вы хотите нарушить авторское право ВСТАВКОЙ!");
        }
        
        #endregion

        #region Структура данных

        const byte DISPLAY_DIGIT = 20;

        private double aOper = 0.0, bOper = 0.0, memRegister = 0.0;    //заныканные переменные для операндов

        public double OperandA   //видимое свойство "Операнд А"
        {
            get { return aOper; }
            set { aOper = value; }
        }

        public double OperandB   //видимое свойство "Операнд Б"
        {
            get { return bOper; }
            set { bOper = value; }
        }

        public double MemRegister   //видимое свойство "Значение в регистре памяти"
        {
            get { return memRegister; }
            set { memRegister = value; }
        }

        private bool pointFlag = false;  //заныканный флаг десятичной точки на экране
        private bool groupFlag = false;  //заныканный флаг группировки цифр в дисплее по три штуки
        private bool signFlag = false;   //заныканный флаг отрицательности числа в дисплеее
        private bool registerFlag = false;   //заныканный флаг наличия числа в регистре

        public bool PointThere    //видимое свойство "Десятичная точка"
        {
            get { return pointFlag; }
            set { pointFlag = value; }
        }

        public bool CiferGroups   //видимое свойство "Группировка цифр"
        {
            get { return groupFlag; }
            set { groupFlag = value; }
        }

        public bool DisplaySign  //видимое свойство "Знак числа на экране"      
        {
            get { return signFlag; }
            set { signFlag = value; }
        }

        public bool RegisterValue  //видимое свойство "Наличие значения в регистре"      
        {
            get { return registerFlag; }
            set { registerFlag = value; }
        }
        
        #endregion

    }
}
