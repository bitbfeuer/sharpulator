﻿namespace Sharpulator
{
    partial class SharpulatorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SharpulatorForm));
            this.PCdisplay = new System.Windows.Forms.TextBox();
            this.SharpyMainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.ToolStripMenuItem_Edit = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem_EditCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem_EditPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem_View = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem_ViewUsual = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem_ViewEngineer = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator_View1 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripMenuItem_ViewDigGroups = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem_Help = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem_HelpSOS = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator_Help1 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripMenuItem_HelpAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.btBackspace = new System.Windows.Forms.Button();
            this.btCE = new System.Windows.Forms.Button();
            this.btC = new System.Windows.Forms.Button();
            this.btSqrt = new System.Windows.Forms.Button();
            this.btPrCnt = new System.Windows.Forms.Button();
            this.btReverse = new System.Windows.Forms.Button();
            this.btEqual = new System.Windows.Forms.Button();
            this.btMplus = new System.Windows.Forms.Button();
            this.btMS = new System.Windows.Forms.Button();
            this.btMR = new System.Windows.Forms.Button();
            this.btMC = new System.Windows.Forms.Button();
            this.btZero = new System.Windows.Forms.Button();
            this.btOne = new System.Windows.Forms.Button();
            this.btFour = new System.Windows.Forms.Button();
            this.btSeven = new System.Windows.Forms.Button();
            this.btSign = new System.Windows.Forms.Button();
            this.btTwo = new System.Windows.Forms.Button();
            this.btFive = new System.Windows.Forms.Button();
            this.btEight = new System.Windows.Forms.Button();
            this.btPoint = new System.Windows.Forms.Button();
            this.btThree = new System.Windows.Forms.Button();
            this.btSix = new System.Windows.Forms.Button();
            this.btNine = new System.Windows.Forms.Button();
            this.btPlus = new System.Windows.Forms.Button();
            this.btMinus = new System.Windows.Forms.Button();
            this.btMultip = new System.Windows.Forms.Button();
            this.btDivide = new System.Windows.Forms.Button();
            this.richTextBox_MemStat = new System.Windows.Forms.RichTextBox();
            this.SharpyMainMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // PCdisplay
            // 
            this.PCdisplay.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.PCdisplay.Enabled = false;
            this.PCdisplay.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PCdisplay.ForeColor = System.Drawing.Color.Transparent;
            this.PCdisplay.Location = new System.Drawing.Point(14, 26);
            this.PCdisplay.MaxLength = 20;
            this.PCdisplay.Name = "PCdisplay";
            this.PCdisplay.ReadOnly = true;
            this.PCdisplay.Size = new System.Drawing.Size(267, 26);
            this.PCdisplay.TabIndex = 0;
            this.PCdisplay.Text = "0,";
            this.PCdisplay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.PCdisplay.WordWrap = false;
            // 
            // SharpyMainMenuStrip
            // 
            this.SharpyMainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItem_Edit,
            this.ToolStripMenuItem_View,
            this.ToolStripMenuItem_Help});
            this.SharpyMainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.SharpyMainMenuStrip.Name = "SharpyMainMenuStrip";
            this.SharpyMainMenuStrip.Size = new System.Drawing.Size(294, 24);
            this.SharpyMainMenuStrip.TabIndex = 1;
            this.SharpyMainMenuStrip.Text = "menuStrip1";
            // 
            // ToolStripMenuItem_Edit
            // 
            this.ToolStripMenuItem_Edit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItem_EditCopy,
            this.ToolStripMenuItem_EditPaste});
            this.ToolStripMenuItem_Edit.Name = "ToolStripMenuItem_Edit";
            this.ToolStripMenuItem_Edit.Size = new System.Drawing.Size(59, 20);
            this.ToolStripMenuItem_Edit.Text = "Правка";
            // 
            // ToolStripMenuItem_EditCopy
            // 
            this.ToolStripMenuItem_EditCopy.Name = "ToolStripMenuItem_EditCopy";
            this.ToolStripMenuItem_EditCopy.Size = new System.Drawing.Size(149, 22);
            this.ToolStripMenuItem_EditCopy.Text = "Копи Ctrl + C";
            this.ToolStripMenuItem_EditCopy.Click += new System.EventHandler(this.ToolStripMenuItem_EditCopy_Click);
            // 
            // ToolStripMenuItem_EditPaste
            // 
            this.ToolStripMenuItem_EditPaste.Name = "ToolStripMenuItem_EditPaste";
            this.ToolStripMenuItem_EditPaste.Size = new System.Drawing.Size(149, 22);
            this.ToolStripMenuItem_EditPaste.Text = "Паста Ctrl + V";
            this.ToolStripMenuItem_EditPaste.Click += new System.EventHandler(this.ToolStripMenuItem_EditPaste_Click);
            // 
            // ToolStripMenuItem_View
            // 
            this.ToolStripMenuItem_View.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItem_ViewUsual,
            this.ToolStripMenuItem_ViewEngineer,
            this.toolStripSeparator_View1,
            this.ToolStripMenuItem_ViewDigGroups});
            this.ToolStripMenuItem_View.Name = "ToolStripMenuItem_View";
            this.ToolStripMenuItem_View.Size = new System.Drawing.Size(39, 20);
            this.ToolStripMenuItem_View.Text = "Вид";
            // 
            // ToolStripMenuItem_ViewUsual
            // 
            this.ToolStripMenuItem_ViewUsual.Checked = true;
            this.ToolStripMenuItem_ViewUsual.CheckOnClick = true;
            this.ToolStripMenuItem_ViewUsual.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ToolStripMenuItem_ViewUsual.Name = "ToolStripMenuItem_ViewUsual";
            this.ToolStripMenuItem_ViewUsual.Size = new System.Drawing.Size(145, 22);
            this.ToolStripMenuItem_ViewUsual.Text = "О-бычный";
            this.ToolStripMenuItem_ViewUsual.Click += new System.EventHandler(this.ToolStripMenuItem_ViewUsual_Click);
            // 
            // ToolStripMenuItem_ViewEngineer
            // 
            this.ToolStripMenuItem_ViewEngineer.CheckOnClick = true;
            this.ToolStripMenuItem_ViewEngineer.Name = "ToolStripMenuItem_ViewEngineer";
            this.ToolStripMenuItem_ViewEngineer.Size = new System.Drawing.Size(145, 22);
            this.ToolStripMenuItem_ViewEngineer.Text = "Engine-рный";
            this.ToolStripMenuItem_ViewEngineer.Click += new System.EventHandler(this.ToolStripMenuItem_ViewEngineer_Click);
            // 
            // toolStripSeparator_View1
            // 
            this.toolStripSeparator_View1.Name = "toolStripSeparator_View1";
            this.toolStripSeparator_View1.Size = new System.Drawing.Size(142, 6);
            // 
            // ToolStripMenuItem_ViewDigGroups
            // 
            this.ToolStripMenuItem_ViewDigGroups.CheckOnClick = true;
            this.ToolStripMenuItem_ViewDigGroups.Name = "ToolStripMenuItem_ViewDigGroups";
            this.ToolStripMenuItem_ViewDigGroups.Size = new System.Drawing.Size(145, 22);
            this.ToolStripMenuItem_ViewDigGroups.Text = "По тройкам";
            this.ToolStripMenuItem_ViewDigGroups.Click += new System.EventHandler(this.ToolStripMenuItem_ViewDigGroups_Click);
            // 
            // ToolStripMenuItem_Help
            // 
            this.ToolStripMenuItem_Help.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItem_HelpSOS,
            this.toolStripSeparator_Help1,
            this.ToolStripMenuItem_HelpAbout});
            this.ToolStripMenuItem_Help.Name = "ToolStripMenuItem_Help";
            this.ToolStripMenuItem_Help.Size = new System.Drawing.Size(65, 20);
            this.ToolStripMenuItem_Help.Text = "Справка";
            // 
            // ToolStripMenuItem_HelpSOS
            // 
            this.ToolStripMenuItem_HelpSOS.Name = "ToolStripMenuItem_HelpSOS";
            this.ToolStripMenuItem_HelpSOS.Size = new System.Drawing.Size(178, 22);
            this.ToolStripMenuItem_HelpSOS.Text = "Спаситя-памагитя!";
            this.ToolStripMenuItem_HelpSOS.Click += new System.EventHandler(this.ToolStripMenuItem_HelpSOS_Click);
            // 
            // toolStripSeparator_Help1
            // 
            this.toolStripSeparator_Help1.Name = "toolStripSeparator_Help1";
            this.toolStripSeparator_Help1.Size = new System.Drawing.Size(175, 6);
            // 
            // ToolStripMenuItem_HelpAbout
            // 
            this.ToolStripMenuItem_HelpAbout.Name = "ToolStripMenuItem_HelpAbout";
            this.ToolStripMenuItem_HelpAbout.Size = new System.Drawing.Size(178, 22);
            this.ToolStripMenuItem_HelpAbout.Text = "И о носороге";
            this.ToolStripMenuItem_HelpAbout.Click += new System.EventHandler(this.ToolStripMenuItem_HelpAbout_Click);
            // 
            // btBackspace
            // 
            this.btBackspace.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btBackspace.ForeColor = System.Drawing.Color.Red;
            this.btBackspace.Location = new System.Drawing.Point(74, 68);
            this.btBackspace.Name = "btBackspace";
            this.btBackspace.Size = new System.Drawing.Size(66, 30);
            this.btBackspace.TabIndex = 3;
            this.btBackspace.Text = "Backspace";
            this.btBackspace.UseVisualStyleBackColor = true;
            this.btBackspace.Click += new System.EventHandler(this.btBackspace_Click);
            // 
            // btCE
            // 
            this.btCE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btCE.ForeColor = System.Drawing.Color.Red;
            this.btCE.Location = new System.Drawing.Point(144, 68);
            this.btCE.Name = "btCE";
            this.btCE.Size = new System.Drawing.Size(66, 30);
            this.btCE.TabIndex = 4;
            this.btCE.Text = "CE";
            this.btCE.UseVisualStyleBackColor = true;
            this.btCE.Click += new System.EventHandler(this.btCE_Click);
            // 
            // btC
            // 
            this.btC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btC.ForeColor = System.Drawing.Color.Red;
            this.btC.Location = new System.Drawing.Point(214, 68);
            this.btC.Name = "btC";
            this.btC.Size = new System.Drawing.Size(66, 30);
            this.btC.TabIndex = 5;
            this.btC.Text = "С";
            this.btC.UseVisualStyleBackColor = true;
            this.btC.Click += new System.EventHandler(this.btC_Click);
            // 
            // btSqrt
            // 
            this.btSqrt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btSqrt.ForeColor = System.Drawing.Color.MediumBlue;
            this.btSqrt.Location = new System.Drawing.Point(244, 107);
            this.btSqrt.Name = "btSqrt";
            this.btSqrt.Size = new System.Drawing.Size(36, 30);
            this.btSqrt.TabIndex = 6;
            this.btSqrt.Text = "sqrt";
            this.btSqrt.UseVisualStyleBackColor = true;
            this.btSqrt.Click += new System.EventHandler(this.btSqrt_Click);
            // 
            // btPrCnt
            // 
            this.btPrCnt.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btPrCnt.ForeColor = System.Drawing.Color.MediumBlue;
            this.btPrCnt.Location = new System.Drawing.Point(244, 147);
            this.btPrCnt.Name = "btPrCnt";
            this.btPrCnt.Size = new System.Drawing.Size(36, 30);
            this.btPrCnt.TabIndex = 7;
            this.btPrCnt.Text = "%";
            this.btPrCnt.UseVisualStyleBackColor = true;
            this.btPrCnt.Click += new System.EventHandler(this.btPrCnt_Click);
            // 
            // btReverse
            // 
            this.btReverse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btReverse.ForeColor = System.Drawing.Color.MediumBlue;
            this.btReverse.Location = new System.Drawing.Point(244, 187);
            this.btReverse.Name = "btReverse";
            this.btReverse.Size = new System.Drawing.Size(36, 30);
            this.btReverse.TabIndex = 8;
            this.btReverse.Text = "1/x";
            this.btReverse.UseVisualStyleBackColor = true;
            this.btReverse.Click += new System.EventHandler(this.btReverse_Click);
            // 
            // btEqual
            // 
            this.btEqual.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btEqual.ForeColor = System.Drawing.Color.Red;
            this.btEqual.Location = new System.Drawing.Point(244, 227);
            this.btEqual.Name = "btEqual";
            this.btEqual.Size = new System.Drawing.Size(36, 30);
            this.btEqual.TabIndex = 9;
            this.btEqual.Text = "=";
            this.btEqual.UseVisualStyleBackColor = true;
            this.btEqual.Click += new System.EventHandler(this.btEqual_Click);
            // 
            // btMplus
            // 
            this.btMplus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btMplus.ForeColor = System.Drawing.Color.Red;
            this.btMplus.Location = new System.Drawing.Point(14, 227);
            this.btMplus.Name = "btMplus";
            this.btMplus.Size = new System.Drawing.Size(36, 30);
            this.btMplus.TabIndex = 13;
            this.btMplus.Text = "M+";
            this.btMplus.UseVisualStyleBackColor = true;
            this.btMplus.Click += new System.EventHandler(this.btMplus_Click);
            // 
            // btMS
            // 
            this.btMS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btMS.ForeColor = System.Drawing.Color.Red;
            this.btMS.Location = new System.Drawing.Point(14, 187);
            this.btMS.Name = "btMS";
            this.btMS.Size = new System.Drawing.Size(36, 30);
            this.btMS.TabIndex = 12;
            this.btMS.Text = "MS";
            this.btMS.UseVisualStyleBackColor = true;
            this.btMS.Click += new System.EventHandler(this.btMS_Click);
            // 
            // btMR
            // 
            this.btMR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btMR.ForeColor = System.Drawing.Color.Red;
            this.btMR.Location = new System.Drawing.Point(14, 147);
            this.btMR.Name = "btMR";
            this.btMR.Size = new System.Drawing.Size(36, 30);
            this.btMR.TabIndex = 11;
            this.btMR.Text = "MR";
            this.btMR.UseVisualStyleBackColor = true;
            this.btMR.Click += new System.EventHandler(this.btMR_Click);
            // 
            // btMC
            // 
            this.btMC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btMC.ForeColor = System.Drawing.Color.Red;
            this.btMC.Location = new System.Drawing.Point(14, 107);
            this.btMC.Name = "btMC";
            this.btMC.Size = new System.Drawing.Size(36, 30);
            this.btMC.TabIndex = 10;
            this.btMC.Text = "MC";
            this.btMC.UseVisualStyleBackColor = true;
            this.btMC.Click += new System.EventHandler(this.btMC_Click);
            // 
            // btZero
            // 
            this.btZero.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btZero.ForeColor = System.Drawing.Color.Green;
            this.btZero.Location = new System.Drawing.Point(74, 227);
            this.btZero.Name = "btZero";
            this.btZero.Size = new System.Drawing.Size(36, 30);
            this.btZero.TabIndex = 17;
            this.btZero.Text = "0";
            this.btZero.UseVisualStyleBackColor = true;
            this.btZero.Click += new System.EventHandler(this.btZero_Click);
            // 
            // btOne
            // 
            this.btOne.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btOne.ForeColor = System.Drawing.Color.Green;
            this.btOne.Location = new System.Drawing.Point(74, 187);
            this.btOne.Name = "btOne";
            this.btOne.Size = new System.Drawing.Size(36, 30);
            this.btOne.TabIndex = 16;
            this.btOne.Text = "1";
            this.btOne.UseVisualStyleBackColor = true;
            this.btOne.Click += new System.EventHandler(this.btOne_Click);
            // 
            // btFour
            // 
            this.btFour.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btFour.ForeColor = System.Drawing.Color.Green;
            this.btFour.Location = new System.Drawing.Point(74, 147);
            this.btFour.Name = "btFour";
            this.btFour.Size = new System.Drawing.Size(36, 30);
            this.btFour.TabIndex = 15;
            this.btFour.Text = "4";
            this.btFour.UseVisualStyleBackColor = true;
            this.btFour.Click += new System.EventHandler(this.btFour_Click);
            // 
            // btSeven
            // 
            this.btSeven.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btSeven.ForeColor = System.Drawing.Color.Green;
            this.btSeven.Location = new System.Drawing.Point(74, 107);
            this.btSeven.Name = "btSeven";
            this.btSeven.Size = new System.Drawing.Size(36, 30);
            this.btSeven.TabIndex = 14;
            this.btSeven.Text = "7";
            this.btSeven.UseVisualStyleBackColor = true;
            this.btSeven.Click += new System.EventHandler(this.btSeven_Click);
            // 
            // btSign
            // 
            this.btSign.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btSign.ForeColor = System.Drawing.Color.MediumBlue;
            this.btSign.Location = new System.Drawing.Point(116, 227);
            this.btSign.Name = "btSign";
            this.btSign.Size = new System.Drawing.Size(36, 30);
            this.btSign.TabIndex = 21;
            this.btSign.Text = "+/-";
            this.btSign.UseVisualStyleBackColor = true;
            this.btSign.Click += new System.EventHandler(this.btSign_Click);
            // 
            // btTwo
            // 
            this.btTwo.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btTwo.ForeColor = System.Drawing.Color.Green;
            this.btTwo.Location = new System.Drawing.Point(116, 187);
            this.btTwo.Name = "btTwo";
            this.btTwo.Size = new System.Drawing.Size(36, 30);
            this.btTwo.TabIndex = 20;
            this.btTwo.Text = "2";
            this.btTwo.UseVisualStyleBackColor = true;
            this.btTwo.Click += new System.EventHandler(this.btTwo_Click);
            // 
            // btFive
            // 
            this.btFive.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btFive.ForeColor = System.Drawing.Color.Green;
            this.btFive.Location = new System.Drawing.Point(116, 147);
            this.btFive.Name = "btFive";
            this.btFive.Size = new System.Drawing.Size(36, 30);
            this.btFive.TabIndex = 19;
            this.btFive.Text = "5";
            this.btFive.UseVisualStyleBackColor = true;
            this.btFive.Click += new System.EventHandler(this.btFive_Click);
            // 
            // btEight
            // 
            this.btEight.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btEight.ForeColor = System.Drawing.Color.Green;
            this.btEight.Location = new System.Drawing.Point(116, 107);
            this.btEight.Name = "btEight";
            this.btEight.Size = new System.Drawing.Size(36, 30);
            this.btEight.TabIndex = 18;
            this.btEight.Text = "8";
            this.btEight.UseVisualStyleBackColor = true;
            this.btEight.Click += new System.EventHandler(this.btEight_Click);
            // 
            // btPoint
            // 
            this.btPoint.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btPoint.ForeColor = System.Drawing.Color.MediumBlue;
            this.btPoint.Location = new System.Drawing.Point(158, 227);
            this.btPoint.Name = "btPoint";
            this.btPoint.Size = new System.Drawing.Size(36, 30);
            this.btPoint.TabIndex = 25;
            this.btPoint.Text = ",";
            this.btPoint.UseVisualStyleBackColor = true;
            this.btPoint.Click += new System.EventHandler(this.btPoint_Click);
            // 
            // btThree
            // 
            this.btThree.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btThree.ForeColor = System.Drawing.Color.Green;
            this.btThree.Location = new System.Drawing.Point(158, 187);
            this.btThree.Name = "btThree";
            this.btThree.Size = new System.Drawing.Size(36, 30);
            this.btThree.TabIndex = 24;
            this.btThree.Text = "3";
            this.btThree.UseVisualStyleBackColor = true;
            this.btThree.Click += new System.EventHandler(this.btThree_Click);
            // 
            // btSix
            // 
            this.btSix.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btSix.ForeColor = System.Drawing.Color.Green;
            this.btSix.Location = new System.Drawing.Point(158, 147);
            this.btSix.Name = "btSix";
            this.btSix.Size = new System.Drawing.Size(36, 30);
            this.btSix.TabIndex = 23;
            this.btSix.Text = "6";
            this.btSix.UseVisualStyleBackColor = true;
            this.btSix.Click += new System.EventHandler(this.btSix_Click);
            // 
            // btNine
            // 
            this.btNine.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btNine.ForeColor = System.Drawing.Color.Green;
            this.btNine.Location = new System.Drawing.Point(158, 107);
            this.btNine.Name = "btNine";
            this.btNine.Size = new System.Drawing.Size(36, 30);
            this.btNine.TabIndex = 22;
            this.btNine.Text = "9";
            this.btNine.UseVisualStyleBackColor = true;
            this.btNine.Click += new System.EventHandler(this.btNine_Click);
            // 
            // btPlus
            // 
            this.btPlus.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btPlus.ForeColor = System.Drawing.Color.Red;
            this.btPlus.Location = new System.Drawing.Point(202, 227);
            this.btPlus.Name = "btPlus";
            this.btPlus.Size = new System.Drawing.Size(36, 30);
            this.btPlus.TabIndex = 29;
            this.btPlus.Text = "+";
            this.btPlus.UseVisualStyleBackColor = true;
            this.btPlus.Click += new System.EventHandler(this.btPlus_Click);
            // 
            // btMinus
            // 
            this.btMinus.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btMinus.ForeColor = System.Drawing.Color.Red;
            this.btMinus.Location = new System.Drawing.Point(202, 187);
            this.btMinus.Name = "btMinus";
            this.btMinus.Size = new System.Drawing.Size(36, 30);
            this.btMinus.TabIndex = 28;
            this.btMinus.Text = "-";
            this.btMinus.UseVisualStyleBackColor = true;
            this.btMinus.Click += new System.EventHandler(this.btMinus_Click);
            // 
            // btMultip
            // 
            this.btMultip.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btMultip.ForeColor = System.Drawing.Color.Red;
            this.btMultip.Location = new System.Drawing.Point(202, 147);
            this.btMultip.Name = "btMultip";
            this.btMultip.Size = new System.Drawing.Size(36, 30);
            this.btMultip.TabIndex = 27;
            this.btMultip.Text = "*";
            this.btMultip.UseVisualStyleBackColor = true;
            this.btMultip.Click += new System.EventHandler(this.btMultip_Click);
            // 
            // btDivide
            // 
            this.btDivide.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btDivide.ForeColor = System.Drawing.Color.Red;
            this.btDivide.Location = new System.Drawing.Point(202, 107);
            this.btDivide.Name = "btDivide";
            this.btDivide.Size = new System.Drawing.Size(36, 30);
            this.btDivide.TabIndex = 26;
            this.btDivide.Text = "/";
            this.btDivide.UseVisualStyleBackColor = true;
            this.btDivide.Click += new System.EventHandler(this.btDivide_Click);
            // 
            // richTextBox_MemStat
            // 
            this.richTextBox_MemStat.Cursor = System.Windows.Forms.Cursors.No;
            this.richTextBox_MemStat.DetectUrls = false;
            this.richTextBox_MemStat.Enabled = false;
            this.richTextBox_MemStat.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.richTextBox_MemStat.Location = new System.Drawing.Point(14, 68);
            this.richTextBox_MemStat.Margin = new System.Windows.Forms.Padding(10);
            this.richTextBox_MemStat.MaximumSize = new System.Drawing.Size(34, 28);
            this.richTextBox_MemStat.MaxLength = 2;
            this.richTextBox_MemStat.MinimumSize = new System.Drawing.Size(34, 28);
            this.richTextBox_MemStat.Multiline = false;
            this.richTextBox_MemStat.Name = "richTextBox_MemStat";
            this.richTextBox_MemStat.ReadOnly = true;
            this.richTextBox_MemStat.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.richTextBox_MemStat.ShortcutsEnabled = false;
            this.richTextBox_MemStat.Size = new System.Drawing.Size(34, 28);
            this.richTextBox_MemStat.TabIndex = 31;
            this.richTextBox_MemStat.TabStop = false;
            this.richTextBox_MemStat.Text = "";
            this.richTextBox_MemStat.WordWrap = false;
            // 
            // SharpulatorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(294, 268);
            this.Controls.Add(this.richTextBox_MemStat);
            this.Controls.Add(this.btPlus);
            this.Controls.Add(this.btMinus);
            this.Controls.Add(this.btMultip);
            this.Controls.Add(this.btDivide);
            this.Controls.Add(this.btPoint);
            this.Controls.Add(this.btThree);
            this.Controls.Add(this.btSix);
            this.Controls.Add(this.btNine);
            this.Controls.Add(this.btSign);
            this.Controls.Add(this.btTwo);
            this.Controls.Add(this.btFive);
            this.Controls.Add(this.btEight);
            this.Controls.Add(this.btZero);
            this.Controls.Add(this.btOne);
            this.Controls.Add(this.btFour);
            this.Controls.Add(this.btSeven);
            this.Controls.Add(this.btMplus);
            this.Controls.Add(this.btMS);
            this.Controls.Add(this.btMR);
            this.Controls.Add(this.btMC);
            this.Controls.Add(this.btEqual);
            this.Controls.Add(this.btReverse);
            this.Controls.Add(this.btPrCnt);
            this.Controls.Add(this.btSqrt);
            this.Controls.Add(this.btC);
            this.Controls.Add(this.btCE);
            this.Controls.Add(this.btBackspace);
            this.Controls.Add(this.PCdisplay);
            this.Controls.Add(this.SharpyMainMenuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SharpulatorForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "bITb Sharpulator";
            this.SharpyMainMenuStrip.ResumeLayout(false);
            this.SharpyMainMenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox PCdisplay;
        private System.Windows.Forms.MenuStrip SharpyMainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_Edit;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_EditCopy;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_EditPaste;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_View;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_ViewUsual;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_ViewEngineer;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator_View1;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_ViewDigGroups;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_Help;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_HelpSOS;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator_Help1;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_HelpAbout;
        private System.Windows.Forms.Button btBackspace;
        private System.Windows.Forms.Button btCE;
        private System.Windows.Forms.Button btC;
        private System.Windows.Forms.Button btSqrt;
        private System.Windows.Forms.Button btPrCnt;
        private System.Windows.Forms.Button btReverse;
        private System.Windows.Forms.Button btEqual;
        private System.Windows.Forms.Button btMplus;
        private System.Windows.Forms.Button btMS;
        private System.Windows.Forms.Button btMR;
        private System.Windows.Forms.Button btMC;
        private System.Windows.Forms.Button btZero;
        private System.Windows.Forms.Button btOne;
        private System.Windows.Forms.Button btFour;
        private System.Windows.Forms.Button btSeven;
        private System.Windows.Forms.Button btSign;
        private System.Windows.Forms.Button btTwo;
        private System.Windows.Forms.Button btFive;
        private System.Windows.Forms.Button btEight;
        private System.Windows.Forms.Button btPoint;
        private System.Windows.Forms.Button btThree;
        private System.Windows.Forms.Button btSix;
        private System.Windows.Forms.Button btNine;
        private System.Windows.Forms.Button btPlus;
        private System.Windows.Forms.Button btMinus;
        private System.Windows.Forms.Button btMultip;
        private System.Windows.Forms.Button btDivide;
        private System.Windows.Forms.RichTextBox richTextBox_MemStat;
    }
}

